import { Component, DestroyRef, OnInit, inject } from '@angular/core';
import {
  NavigationEnd,
  Router,
  RouterLink,
  RouterOutlet,
} from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { filter } from 'rxjs/operators';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { loadClients } from './store/client/client.actions';
import { Store } from '@ngrx/store';
import { ClientService } from './services/client.service';
import { downloadExcel } from '../data/download.function';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private clientService = inject(ClientService);
  // private store = inject(Store);

  public showGoBack!: boolean;
  public showFiller = false;

  ngOnInit(): void {
    // this.store.dispatch(loadClients());
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        takeUntilDestroyed(this.destroyRef)
      )
      .subscribe((value: any) => {
        if ((value.url as string) !== '/') {
          this.showGoBack = true;
          return;
        }
        this.showGoBack = false;
      });
  }

  exportExcel() {
    this.clientService.exportPatients().subscribe({
      next: (response: ArrayBuffer) => {
        downloadExcel(response, 'pacientes.xlsx');
      },
      error: (error) => {
        console.error('Error al exportar el archivo Excel:', error);
      },
    });
  }
}
