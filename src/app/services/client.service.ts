import { Injectable, inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Patient } from '../../data/interfaces';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  private apiUrl = environment.apiUrl;

  private http = inject(HttpClient);

  getClients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(`${this.apiUrl}/client`);
  }
  getClientById(id): Observable<Patient> {
    return this.http.get<Patient>(`${this.apiUrl}/client/${id}`);
  }

  addClient(client: Patient): Observable<Patient> {
    return this.http.post<Patient>(`${this.apiUrl}/client`, client);
  }

  updateClient(id, client: Patient): Observable<Patient> {
    return this.http.put<Patient>(`${this.apiUrl}/client/${id}`, client);
  }

  deleteClient(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/client/${id}`);
  }

  exportPatients() {
    return this.http.get(`${this.apiUrl}/excel/export`, {
      responseType: 'arraybuffer',
      headers: new HttpHeaders().append(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      ),
    });
  }
}
