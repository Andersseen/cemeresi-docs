import {
  type AfterViewInit,
  Component,
  ViewChild,
  inject,
  TemplateRef,
  effect,
  Signal,
  signal,
  ElementRef,
} from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { Store, select } from '@ngrx/store';
import { selectClients } from '../../store/client/client.store';
import type { Patient } from '../../../data/interfaces';
import { MatIcon } from '@angular/material/icon';
import { Router } from '@angular/router';
import {
  MatDialog,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
} from '@angular/material/dialog';
import { deleteClient, loadClients } from '../../store/client/client.actions';
import { tableRowAnimation } from './table.animation';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ClientService } from '../../services/client.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: 'table.template.html',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIcon,
    MatButtonModule,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatProgressBarModule,
  ],
  animations: [tableRowAnimation],
})
export default class TableComponent {
  // private store = inject(Store);
  private router = inject(Router);
  public dialog = inject(MatDialog);
  private clientService = inject(ClientService);

  public displayedColumns = [
    'actions',
    'id',
    'name',
    'lastName',
    'phone',
    'email',
    'notes',
  ];
  public dataSource: MatTableDataSource<Patient>;
  private clients = signal([] as Patient[]);

  // public clients = this.store.selectSignal(selectClients);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('input') input!: ElementRef;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('modalTemplate') modalTemplate!: TemplateRef<any>;

  constructor() {
    this.dataSource = new MatTableDataSource(this.clients());
    effect(() => {
      this.dataSource = new MatTableDataSource(this.clients());
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    this.clientService
      .getClients()
      .subscribe((value) => this.clients.set(value));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editClient(id: string) {
    this.router.navigate(['/form', id]);
  }
  deleteClient(id: string) {
    const dialogRef = this.dialog.open(this.modalTemplate, {
      restoreFocus: false,
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (result === 'ok')
        this.clientService
          .deleteClient(id)
          .pipe(switchMap(() => this.clientService.getClients()))
          .subscribe((value) => {
            (this.input.nativeElement as HTMLInputElement).value = '';
            this.clients.set(value);
          });
    });

    // dialogRef.afterClosed().subscribe((result: string) => {
    //   if (result === 'ok') this.store.dispatch(deleteClient({ id }));
    // });
  }
  historicalPatient(id) {
    this.router.navigate(['/historical', id]);
  }
}
