import { trigger, animate, transition, style } from '@angular/animations';

export const tableRowAnimation = trigger('tableRowAnimation', [
  transition(':enter', [
    style({ opacity: 0, transform: 'translateY(-10px)' }),
    animate('0.2s', style({ opacity: 1, transform: 'translateY(0)' })),
  ]),
  transition(':leave', [
    animate(
      '0.2s',
      style({
        background: '#673ab7',
        borderBottomColor: '#673ab7',
        opacity: 0,
        transform: 'translateX(-100%)',
        'box-shadow': 'none',
      })
    ),
  ]),
]);
