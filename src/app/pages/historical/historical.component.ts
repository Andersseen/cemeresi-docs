import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  WritableSignal,
  inject,
  signal,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { ClientService } from '../../services/client.service';
import { Patient } from '../../../data/interfaces';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { HistoricalService } from './historical.service';
import { switchMap } from 'rxjs';
import { downloadPDF } from '../../../data/download.function';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
@Component({
  selector: 'app-historical',
  templateUrl: 'historical.template.html',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ReactiveFormsModule, MatButtonModule, MatInputModule],
  providers: [
    HistoricalService,
    // { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' },
  ],
})
export default class HistoricalComponent implements OnInit {
  private clientService = inject(ClientService);
  private route = inject(ActivatedRoute);
  private historicalService = inject(HistoricalService);
  private snackBar = inject(MatSnackBar);

  public historicalContent = new FormControl('');

  public patient: WritableSignal<Patient | null> = signal(null);
  public patientId!: string | null;

  ngOnInit(): void {
    this.patientId = this.route.snapshot.paramMap.get('id');
    this.clientService
      .getClientById(this.patientId)
      .pipe(
        switchMap((patient) => {
          this.patient.set(patient);
          return this.historicalService.getHistoricalById(this.patientId);
        })
      )
      .subscribe((value) => {
        if (!value) return;
        this.historicalContent.setValue(value.history);
      });
  }

  saveHistorical() {
    this.historicalService
      .saveHistorical(this.patientId, this.historicalContent.value)
      .subscribe({
        next: () =>
          this.openSnackBar('El histórico se ha guardado correctamente', 'OK'),
        error: () =>
          this.openSnackBar(
            'Se ha producido un error al guardar el historial',
            'OK'
          ),
      });
  }

  generatePDF() {
    this.historicalService.generateHistoricalPDF(this.patientId).subscribe({
      next: (response: ArrayBuffer) =>
        downloadPDF(
          response,
          `histórico_de_${this.patient()?.name}_${
            this.patient()?.firstLastName
          }.pdf`
        ),
      error: (error) => {
        console.error('Error al exportar el archivo PDF:', error);
      },
    });
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }
}
