import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class HistoricalService {
  private http = inject(HttpClient);
  private apiUrl = environment.apiUrl;

  public getHistoricalById(id): Observable<any> {
    return this.http.get(`${this.apiUrl}/historical/${id}`);
  }

  public saveHistorical(id, content) {
    return this.http.post(`${this.apiUrl}/historical/${id}`, { content });
  }
  public generateHistoricalPDF(id) {
    return this.http.get(`${this.apiUrl}/historical/${id}/pdf`, {
      responseType: 'arraybuffer',
      headers: new HttpHeaders().append('Content-Type', 'application/pdf'),
    });
  }
}
