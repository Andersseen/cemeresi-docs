import { OnInit, Component, inject, signal } from '@angular/core';
import { MatCard, MatCardContent } from '@angular/material/card';
import { MatInput } from '@angular/material/input';
import {
  MatFormField,
  MatLabel,
  MatSuffix,
} from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatButton } from '@angular/material/button';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import type { Patient } from '../../../data/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-form-client',
  templateUrl: 'form-client.template.html',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatCard,
    MatInput,
    MatFormField,
    MatLabel,
    MatCardContent,
    MatIcon,
    MatSuffix,
    MatButton,
  ],
})
export default class FormClientComponent implements OnInit {
  // private store = inject(Store);
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private clientService = inject(ClientService);

  private clientId: string | null = null;
  // public clientList = this.store.selectSignal(selectClients);
  public clientForm: FormGroup;
  private clients = signal([] as Patient[]);

  constructor(private snackBar: MatSnackBar) {
    this.clientForm = inject(FormBuilder).group({
      name: ['', Validators.required],
      firstLastName: ['', Validators.required],
      secondLastName: [''],
      phone: [''],
      email: [''],
      notes: [''],
    });
  }
  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get('id');
    this.clientService.getClients().subscribe((value) => {
      this.clients.set(value);
      if (!this.clientId) return;
      const findClient = this.clients().find(
        (client) => client.id === Number(this.clientId)
      );
      if (findClient) this.clientForm.patchValue(findClient);
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  submitForm() {
    if (this.clientForm.invalid) return;
    const client = {
      name: this.clientForm.get('name')!.value ?? '',
      firstLastName: this.clientForm.get('firstLastName')!.value ?? '',
      secondLastName: this.clientForm.get('secondLastName')!.value ?? '',
      phone: this.clientForm.get('phone')?.value ?? '',
      email: this.clientForm.get('email')?.value ?? '',
      notes: this.clientForm.get('notes')?.value ?? '',
    };

    if (this.clientId) {
      // this.store.dispatch(updateClient({ client } as any));
      this.clientService.updateClient(this.clientId, client as any).subscribe();
      this.openSnackBar('El cliente se ha editado correctamente', 'OK');
    } else {
      this.clientService.addClient(client as any).subscribe();
      // this.store.dispatch(addClient({ client } as any));
      this.openSnackBar('El cliente se ha guardado correctamente', 'OK');
    }
    this.clientForm.reset();
    this.router.navigate(['']);
  }
}
