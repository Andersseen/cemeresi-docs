import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient } from '@angular/common/http';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { appReducers } from './store/app.store';
import { provideEffects } from '@ngrx/effects';
import { ClientEffects } from './store/client/client.effects';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(),
    provideAnimationsAsync(),
    // provideStore(appReducers),
    // provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode() }),
    // provideEffects(ClientEffects),
  ],
};
