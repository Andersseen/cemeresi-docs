import { ActionReducerMap } from '@ngrx/store';
import { clientReducer } from './client/client.reducer';
import { ClientState } from './client/client.state';

export interface AppState {
  clients: ClientState;
}

export const appReducers: ActionReducerMap<AppState> = {
  clients: clientReducer,
};
