import { Patient } from '../../../data/interfaces';

export interface ClientState {
  clients: Patient[];
  selectedClient: Patient | null;
  error: any;
}

export const initialClientState: ClientState = {
  clients: [],
  selectedClient: null,
  error: null,
};
