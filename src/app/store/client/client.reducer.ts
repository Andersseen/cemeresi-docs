import { createReducer, on } from '@ngrx/store';
import { initialClientState } from './client.state';
import * as ClientActions from './client.actions';

export const clientReducer = createReducer(
  initialClientState,
  on(ClientActions.loadClientsSuccess, (state, { clients }) => ({
    ...state,
    clients: clients,
  })),
  on(ClientActions.loadClientsFailure, (state, { error }) => ({
    ...state,
    error: error,
  })),
  on(ClientActions.addClient, (state, { client }) => ({
    ...state,
    clients: [...state.clients, client],
  })),
  on(ClientActions.updateClient, (state, { client }) => ({
    ...state,
    clients: state.clients.map((existingClient) =>
      existingClient.id === client.id ? client : existingClient
    ),
  })),
  on(ClientActions.deleteClient, (state, { id }) => ({
    ...state,
    clients: state.clients.filter((client) => client.id !== Number(id)),
  }))
);
