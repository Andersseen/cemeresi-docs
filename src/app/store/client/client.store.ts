import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ClientState } from './client.state';

const selectClientsFeature = createFeatureSelector<ClientState>('clients');

export const selectClients = createSelector(
  selectClientsFeature,
  (state: ClientState) => state
);
