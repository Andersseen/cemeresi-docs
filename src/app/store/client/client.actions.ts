import { createAction, props } from '@ngrx/store';
import type { Patient } from '../../../data/interfaces';

export const loadClients = createAction('[Client] Load Clients');
export const loadClientsSuccess = createAction(
  '[Client] Load Clients Success',
  props<{ clients: Patient[] }>()
);
export const loadClientsFailure = createAction(
  '[Client] Load Clients Failure',
  props<{ error: any }>()
);

export const addClient = createAction(
  '[Client] Add Client',
  props<{ client: Patient }>()
);
export const updateClient = createAction(
  '[Client] Update Client',
  props<{ client: Patient }>()
);
export const deleteClient = createAction(
  '[Client] Delete Client',
  props<{ id: string }>()
);
