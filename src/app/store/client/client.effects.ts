// client.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as ClientActions from './client.actions';
import { ClientService } from '../../services/client.service';

@Injectable()
export class ClientEffects {
  loadClients$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ClientActions.loadClients),
      switchMap(() =>
        this.clientService.getClients().pipe(
          map((clients) => ClientActions.loadClientsSuccess({ clients })),
          catchError((error) => of(ClientActions.loadClientsFailure({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private clientService: ClientService
  ) {}
}
