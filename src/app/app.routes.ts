import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./pages/table/table.component'),
  },
  {
    path: 'form',
    loadComponent: () => import('./pages/form-client/form-client.component'),
  },
  {
    path: 'form/:id',
    loadComponent: () => import('./pages/form-client/form-client.component'),
  },

  {
    path: 'historical/:id',
    loadComponent: () => import('./pages/historical/historical.component'),
  },
];
