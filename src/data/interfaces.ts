export interface Patient {
  id: number;
  name: string;
  firstLastName: string;
  secondLastName: string;
  phone?: string;
  email?: string;
  notes?: string;
}
export interface Historical {
  id: number;
  patientId: number;
  history: string;
}
