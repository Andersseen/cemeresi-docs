export const CLIENTS = [
  {
    id: '1',
    name: 'John',
    lastName: 'Doe',
    phone: '555-1234',
    email: 'john@example.com',
    notes: 'Regular customer',
  },
  {
    id: '2',
    name: 'Jane',
    lastName: 'Smith',
    phone: '555-5678',
    email: 'jane@example.com',
    notes: 'VIP customer',
  },
  {
    id: '3',
    name: 'Michael',
    lastName: 'Johnson',
    phone: '555-9876',
    email: 'michael@example.com',
    notes: 'New customer',
  },
  {
    id: '4',
    name: 'Emily',
    lastName: 'Brown',
    phone: '555-4321',
    email: 'emily@example.com',
    notes: 'Frequent buyer',
  },
  {
    id: '5',
    name: 'David',
    lastName: 'Lee',
    phone: '555-8765',
    email: 'david@example.com',
    notes: 'Preferred customer',
  },
  {
    id: '6',
    name: 'Sarah',
    lastName: 'Taylor',
    phone: '555-2345',
    email: 'sarah@example.com',
    notes: 'Corporate client',
  },
  {
    id: '7',
    name: 'Daniel',
    lastName: 'Wilson',
    phone: '555-5432',
    email: 'daniel@example.com',
    notes: 'Potential lead',
  },
  {
    id: '8',
    name: 'Olivia',
    lastName: 'Martinez',
    phone: '555-6789',
    email: 'olivia@example.com',
    notes: 'Referral from existing client',
  },
  {
    id: '9',
    name: 'William',
    lastName: 'Garcia',
    phone: '555-7890',
    email: 'william@example.com',
    notes: 'Interested in new products',
  },
  {
    id: '10',
    name: 'Ava',
    lastName: 'Hernandez',
    phone: '555-8901',
    email: 'ava@example.com',
    notes: 'Previous complaint resolved',
  },
];
