export function downloadPDF(binaryData: ArrayBuffer, fileName: string): void {
  const file: Blob = new Blob([binaryData], { type: 'application/pdf' });
  const url: string = window.URL.createObjectURL(file);
  const anchorElement: HTMLAnchorElement = document.createElement('a');
  anchorElement.download = fileName;
  anchorElement.href = url;
  anchorElement.dispatchEvent(
    new MouseEvent('click', { bubbles: true, cancelable: true, view: window })
  );
}

export function downloadExcel(binaryData: ArrayBuffer, fileName: string): void {
  const file: Blob = new Blob([binaryData], {
    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });
  const url: string = window.URL.createObjectURL(file);
  const anchorElement: HTMLAnchorElement = document.createElement('a');
  anchorElement.download = fileName;
  anchorElement.href = url;
  anchorElement.dispatchEvent(
    new MouseEvent('click', { bubbles: true, cancelable: true, view: window })
  );
}
